
#include <iostream>
#include <math.h>


class Vector
{
public:
    Vector() : x(0), y(0), z(0)
    {}

    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}

    void ShowModule()
    {
        VectorModule = sqrt( pow(x,2) + pow(y,2) + pow(z,2) );
        std::cout << "Module of vector: " << VectorModule << std::endl;
    }

private:
    double x;
    double y;
    double z;
    double VectorModule;
};

class Testing
{
public:
    Testing() : a(0), b(0), c(0)
    {}

    Testing(int _a, int _b, int _c) : a(_a), b(_b), c(_c)
    {}

    void ShowSquareSum()
    {
        SquareSum = pow( (a+b+c), 2 );
        std::cout << "Square of Sum: " << SquareSum << std::endl;
    }

private:
    int a, b, c, SquareSum;
};


int main()
{
    Vector v1(12.03, 3.12, 4.099);
    v1.ShowModule();

    Testing t1(4, 2, 10);
    t1.ShowSquareSum();

}